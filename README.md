# Proyecto_GitM8_aac_jma

## Vectors

La clase vectores es la clase que realiza diferents funciones matematicas sobre vectores.
Para utilizar esta clase una vez la tengamos deberemos crear una intancia de la clase y llamar al metodo deseado en ese momento:

- include "vectores.php";
- $V = new Vector();
  
## Matrius

La clase matrius es la clase utilizada para realizar diferentes operaciones matemáticas sobre matrices.
Para utilizar esta clase una vez la tengamos, deberemos crear una instancia y llamar al método que queramos:

- include "matrius.php";
- $m = new Matrius();
